# Cas d'utilisation N° 2 :  Passer la commande

Niveau Objectif utilisateur

##	Description

- Le client s'installe à une table, regarde le menu et passe sa commande auprès du serveur.


> **Niveau** : Objectif utilisateur   
> **Déclencheur** : Le client entre dans le restaurant 
> **Acteur Primaire**: Client
> **Acteurs secondaires**: Maitre d'hôtel, serveur  
> **Parties Prenantes concernées** : Barman, Cuisinier  
 
 
## Preconditions

Il faut de la place dans le restaurant

## Scenario Nominal

FIXME_[tout ce passe bien c'est le scénario parfait .]_

1.	Le client s'installe à une table
2.	Il regarde le menu
3.	Fait signe au serveur qu'il a choisi
4.	Donne sa commande


## Post Conditions
### Conditions de succès 
Un serveur prend la commande

### Minimal Guarantees
### Conditions final en cas d'échec
### Frequence
Chaque client prend généralement qu'une seule commande
### Besoins Spéciaux (optionel)  
### Performance  
###	Security  
###	Usability / Accessibility  
###	Other  

##	Problèmes et étapes suivantes 


TBR