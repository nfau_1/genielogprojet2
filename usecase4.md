# Cas d'utilisation N° 4 : Récupère une facture

Niveau Objectif client

##	Description

Le client vient pour payer et le caissier récupère la facture

> **Niveau** : Objectif client
> **Déclencheur** : le caissier récupère la facture pour faire payer le client
> **Acteur Primaire**: caissier  
> **Acteurs secondaires**: client, Pizzacompta
> **Parties Prenantes concernées** : Aucun
 
 
## Preconditions

Le caissier doit connaître la table et les données dans Pizzacompta doivent être à jour


## Scenario Nominal

1.	Le caissier entre le numéro de la table pour récupérer le ticket
2.	L'application va récupérer les bonnes informations dans Pizzacompta
3.	Le caissier reçoit le ticket, et demande au client de payer
4.	Le client paye

###	Extensions
Le client ne correspond pas à la table renseignée

## Post Conditions
### Conditions de succès 
Les données doivent être à jour dans Pizzacompta

### Minimal Guarantees

### Conditions final en cas d'échec

### Frequence
A chaque fois qu'un client veut payer. Entre 50 et 100 fois par jour.
### Besoins Spéciaux (optionel)  
### Performance  
###	Security  
###	Usability / Accessibility  
###	Other  

##	Problèmes et étapes suivantes  

TBR