# Cas d'utilisation N° 3 :  Attribuer les tables aux clients

Niveau Objectif utilisateur

##	Description

Le maître d'hotel va placer les clients sur les tables disponibles

> **Niveau** : objectif utilisateur
> **Déclencheur** : Un client arrive
> **Acteur Primaire**: Maitre d'hotel  
> **Acteurs secondaires**: Aucun   
> **Parties Prenantes concernées** : Client   
 
 
## Preconditions

Un nouveau client doit arriver et des places doivent être disponibles


## Scenario Nominal

1.	Le maitre d'hotel regarde les tables disponibles sur l'application
2.	Si une table est disponible, il place les clients à cette table
3.  De plus, il met l'information comme quoi la table est utilisée dans l'application

###	Extensions


## Post Conditions
### Conditions de succès 
Il faut une table de libre et que les données soient à jour

### Minimal Guarantees
### Conditions final en cas d'échec
### Frequence
Il fera cela très souvent, pour chaque client, donc au moins 50 fois à 100 par jour

### Besoins Spéciaux (optionel)  
### Performance  
###	Security  
###	Usability / Accessibility  
###	Other  

##	Problèmes et étapes suivantes

TBR