# Cas d'utilisation N° 1 :  Prise de commande

Niveau Objectif utilisateur

##	Description

Le serveur va prendre la commande du client pour les envoyer aux cuisiniers et au barman grâce à l'application.

> **Niveau** : objectif utilisateur 
> **Déclencheur** : Un client fait sa commande
> **Acteur Primaire**: Serveur   
> **Acteurs secondaires**: Client   
> **Parties Prenantes concernées** : barman, cuisiniers
 
 
## Preconditions

- avoir un client
- l'application fonctionne


## Scenario Nominal

1.	Le serveur indique la table sur l'application
2.	Le client lui donne sa commande
3.	Le seveur entre les informations dans l'application
4.	Il Valide la commande

###	Extensions


## Post Conditions
### Conditions de succès 
le serveur a choisi la bonne table et ne s'est pas trompé dans la commande

### Minimal Guarantees
Le client doit recevoir la commande qu'il a commandé

### Conditions final en cas d'échec
le serveur s'est trompé de table
### Frequence 
Très souvent, au moins 50 fois à 100 fois par jour
### Besoins Spéciaux (optionel)  
### Performance  
###	Security  
###	Usability / Accessibility  
###	Other  

##	Problèmes et étapes suivantes  

TBR